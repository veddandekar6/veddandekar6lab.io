import React from "react";
import TopNav from "./components/navbar";
import Splash from "./components/splash";
import About from "./components/about";
import Projects from "./components/projects";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
	return (
		<Router>
			<TopNav />
			<Route path="/" exact component={Splash} />
			<Route path="/about" exact component={About} />
			<Route path="/projects" exact component={Projects} />
		</Router>
	);
}

export default App;
