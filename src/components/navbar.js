import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
function TopNav() {
	return (
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
			<Link to="/">
				<Navbar.Brand href="/">Portfolio</Navbar.Brand>
			</Link>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="mr-auto">
					<Link className="text-decoration-none" to="about">
						<Nav.Link href="about">About</Nav.Link>
					</Link>
					<Link className="text-decoration-none" to="projects">
						<Nav.Link href="projects">Projects</Nav.Link>
					</Link>
				</Nav>
				<Nav>
					<Button
						variant="outline-secondary"
						href="https://drive.google.com/file/d/1bE_-yotBJr8VhAMXDf0lbY2X7qcRM8w2/view?usp=sharing"
						target="_blank"
					>
						Resume
					</Button>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}

export default TopNav;
