import React from "react";
import { Container, Button } from "react-bootstrap";
import Fade from "react-reveal/Fade";
import ContactBar from "./contactBar";
import TypeText from "./typeText";

function Splash(props) {
	return (
		<Fade>
			<Container className="text-center" fluid>
				<Fade delay={12000}>
					<hr
						style={{
							border: "2px solid white",
							width: "50%",
							marginTop: "15vh",
						}}
					/>
				</Fade>
				<TypeText />
				<Fade delay={12000}>
					<hr
						style={{
							border: "2px solid white",
							width: "50%",
							marginBottom: "5vh",
						}}
					/>
				</Fade>
				<Fade delay={12000}>
					<Button
						className="my-5"
						size="lg"
						variant="outline-secondary"
						href="https://drive.google.com/file/d/1bE_-yotBJr8VhAMXDf0lbY2X7qcRM8w2/view?usp=sharing"
						target="_blank"
					>
						Resume
					</Button>
				</Fade>
				<br />
				<Fade delay={12000}>
					<ContactBar />
				</Fade>
			</Container>
		</Fade>
	);
}

export default Splash;
