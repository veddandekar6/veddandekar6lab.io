import React from "react";
import { Row, Col } from "react-bootstrap";

function zoomIn(e) {
	e.target.style.transition = "1s";
	e.target.style.transform = "scale(1.5)";
}

function zoomOut(e) {
	e.target.style.transform = "scale(1)";
}

function ContactBar() {
	return (
		<Row>
			<Col xs={4}>
				<a
					href="https://www.linkedin.com/in/ved-dandekar"
					target="_blank"
					rel="noopener noreferrer"
				>
					<img
						src="https://www.pinclipart.com/picdir/big/100-1003028_zig-zag-clipart.png"
						width="50px"
						alt="LinkedIn"
						onMouseEnter={zoomIn}
						onMouseLeave={zoomOut}
					/>
				</a>
			</Col>
			<Col xs={4}>
				<a
					href="https://www.gitlab.com/veddandekar6/"
					target="_blank"
					rel="noopener noreferrer"
				>
					<img
						src="https://www.pinclipart.com/picdir/big/341-3418221_diamond-plate-clipart.png"
						style={{ filter: "invert(1)" }}
						width="60px"
						alt="GitLab"
						onMouseEnter={zoomIn}
						onMouseLeave={zoomOut}
					/>
				</a>
			</Col>
			<Col xs={4}>
				<a href="mailto:veddandekar6@gmail.com">
					<img
						src="https://www.pinclipart.com/picdir/big/498-4989718_mail-letter-clipart.png"
						style={{ filter: "invert(1)" }}
						width="60px"
						alt="Email"
						onMouseEnter={zoomIn}
						onMouseLeave={zoomOut}
					/>
				</a>
			</Col>
		</Row>
	);
}

export default ContactBar;
