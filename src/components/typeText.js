import React from "react";
import Typist from "react-typist";

function TypeText() {
  let cursor = {
    show: true,
    element: "|",
    hideWhenDone: true,
    hideWhenDoneDelay: 300,
  };

  return (
    <div
      style={{
        fontFamily: "Rubik Mono One",
        fontSize: "1.8em",
        color: "white",
      }}
    >
      <Typist
        avgTypingDelay={85}
        startDelay={2000}
        cursor={cursor}
        // onTypingDone={setOpen()}
      >
        Hey!
        <Typist.Delay ms={700} /> <br />
        I'm Ved
        <Typist.Delay ms={700} /> <br /> <br />
        Developer
        <Typist.Delay ms={700} /> <br />
        By degree
        <Typist.Backspace count={6} delay={500} />
        profession
        <Typist.Backspace count={9} delay={500} />
        ASSION
        {/* <Typist.Delay ms={700} />. */}
      </Typist>
    </div>
  );
}

export default TypeText;
