import React from "react";
import ProjectCard from "./projectCard";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import { Container, Row, Col } from "react-bootstrap";

function Projects() {
	const Milaap = {
		title: "Milaap",
		desc:
			"A peer-to-peer shared video experience app using WebRTC. Supports audio-video calling with screen and document sharing along with synchronized YouTube video playback.",
		img: "https://i.ibb.co/yQBvcsW/milaap.png",
		link: "https://www.milaap.video/",
	};

	const CoronaStatsIndia = {
		title: "Corona Stats - India",
		desc:
			"A neat and simple Progressive Web App that shows the state-wise count of confirmed victims, recoveries and deaths. Several clean graphical representations comparing the spread across different states are also provided.",
		img: "https://i.ibb.co/gmpwd4c/coronstats.png",
		link: "https://gitlab.com/veddandekar6/coronaStats-India",
	};

	const collaBRO = {
		title: "collaBRO",
		desc:
			"collaBRO is an online web-based collaboration tool, similar to Trello, developed with simplicity and productivity in mind. The project boasts a simple to use package that supports all necessary functionality.",
		img: "https://gitlab.com/veddandekar6/collaBRO/-/raw/master/media/ss2.jpg",
		link: "https://gitlab.com/veddandekar6/collaBRO",
	};

	const FTP = {
		title: "FTP",
		desc:
			"FTP client & server in Python, strictly following the FTP RFC, supporting various commands and features such as simultaneous clients & compatibility with foreign apps.",
		img:
			"https://blog.ipswitch.com/hubfs/Featured%20Images/what-is-file-transfer-protocol.png",
		link: "https://gitlab.com/veddandekar6/ftp-client-server",
	};

	const Compression = {
		title: "Compression",
		desc:
			"Implemented two compression and decompression algorithms in C – Huffman coding and LZW.",
		img: "https://veddandekar6.gitlab.io/intro/images/compression.jpg",
		link: "https://gitlab.com/veddandekar6/compression",
	};

	const PubData = {
		title: "Publication Data",
		desc:
			"Created a centralized online repository for college where students can submit their publications and the faculties can verify, approve and fund them.",
		img: "https://veddandekar6.gitlab.io/intro/images/pubdata.jpg",
	};

	let zoomDelay = 100;

	return (
		<Container>
			<Fade>
				<Row className="my-5">
					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={Milaap} />
						</Zoom>
					</Col>
					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={CoronaStatsIndia} />
						</Zoom>
					</Col>

					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={collaBRO} />
						</Zoom>
					</Col>

					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={FTP} />
						</Zoom>
					</Col>

					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={PubData} />
						</Zoom>
					</Col>

					<Col
						style={{ display: "flex", justifyContent: "center" }}
						className="mt-4"
						xs={12}
						md={4}
					>
						<Zoom delay={zoomDelay}>
							<ProjectCard data={Compression} />
						</Zoom>
					</Col>
				</Row>
			</Fade>
		</Container>
	);
}

export default Projects;
