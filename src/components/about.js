import React from "react";
import Fade from "react-reveal/Fade";
import {
	VerticalTimeline,
	VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { Jumbotron, Container, Row, Col } from "react-bootstrap";

function About() {
	return (
		<Container>
			<Fade>
				<Jumbotron
					className="mt-4 p-5 text-white"
					style={{ backgroundColor: "rgb(52,58,64)", color: "white" }}
				>
					<Row>
						<Col className="text-center" md={3}>
							<img
								style={{ borderRadius: "200px", width: "70%" }}
								src={
									"https://media-exp1.licdn.com/dms/image/C5103AQEHkOBdj5Pmpg/profile-displayphoto-shrink_400_400/0?e=1608163200&v=beta&t=GBENYZ1376zLYVNTgE2F-jBhfqYYyNeC8DWqn06QzCk"
								}
								alt="Ved's display"
							/>
						</Col>
						<Col className="mt-5 text-center" xs={12} md={9}>
							A passionate student with a specific interest in understanding
							technology over just using it. <br />
							Currently in search of a place where I can not only learn but also
							apply my skills.
						</Col>
					</Row>
				</Jumbotron>

				<h2 className="text-center text-white pt-4">Previous Experience</h2>
				<VerticalTimeline>
					<VerticalTimelineElement
						className="vertical-timeline-element--work"
						contentStyle={{
							background: "rgb(62,68,74)",
							borderTop: "2px solid rgb(33, 150, 243)",
							color: "#FFF",
						}}
						position={"left"}
						date="May 2020 - July 2020"
						dateClassName="text-white"
						layout="2-column"
						iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
					>
						<h3 className="vertical-timeline-element-title">Credit Suisse</h3>
						<h6 className="vertical-timeline-element-subtitle">
							Bangalore, Karnataka
						</h6>
						<hr />
						<p>
							⦿ Developed a unique NLP based algorithm using Python-NLTK for a
							chatbot to accurately extract intents and contents from user
							queries. <br />
							<br />
							⦿Provided an extensive front-end UI using React to allow admins to
							easily train and extend bot capabilities on the fly. <br />
							<br />
							⦿Automated and streamlined entire CI/CD process using Docker,
							Jenkins and OpenShift.
						</p>
					</VerticalTimelineElement>
					<VerticalTimelineElement
						className="vertical-timeline-element--work"
						contentStyle={{
							background: "rgb(62,68,74)",
							borderTop: "2px solid rgb(33, 150, 243)",
							color: "#FFF",
						}}
						position={"right"}
						date="May 2019 - July 2019"
						dateClassName="text-white"
						layout="2-column"
						iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
					>
						<h3 className="vertical-timeline-element-title">Cytius</h3>
						<h6 className="vertical-timeline-element-subtitle">
							Pune, Maharashtra
						</h6>
						<hr />
						<p>
							⦿ Exposed to several network security tools such as Wireshark,
							Snort and Suricata. <br />
							<br />⦿ Developed a sentiment analysis model with 74% accuracy.
						</p>
					</VerticalTimelineElement>
				</VerticalTimeline>
				<hr className="bg-white m-5" />
				<h2 className="text-center text-white">Education</h2>
				<VerticalTimeline>
					<VerticalTimelineElement
						className="vertical-timeline-element--education"
						contentStyle={{
							background: "rgb(62,68,74)",
							borderTop: "2px solid rgb(33, 150, 243)",
							color: "#FFF",
						}}
						position={"left"}
						date="2017 - 2021"
						dateClassName="text-white"
						layout="2-column"
						iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
					>
						<h3 className="vertical-timeline-element-title">
							College of Engineering Pune
						</h3>
						<h6 className="vertical-timeline-element-subtitle">
							Pune, Maharashtra
						</h6>
						<hr />
						<p>
							⦿ Pursuing B.Tech. IT <br />⦿ Current CGPA - 8.59
						</p>
					</VerticalTimelineElement>
					<VerticalTimelineElement
						className="vertical-timeline-element--education"
						contentStyle={{
							background: "rgb(62,68,74)",
							borderTop: "2px solid rgb(33, 150, 243)",
							color: "#FFF",
						}}
						position={"right"}
						date="2015 - 2017"
						dateClassName="text-white"
						layout="2-column"
						iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
					>
						<h3 className="vertical-timeline-element-title">
							Sunrise English Pvt School
						</h3>
						<h6 className="vertical-timeline-element-subtitle">
							Abu Dhabi, UAE
						</h6>
						<hr />
						<p>
							⦿ School topper with 98/100 in Comp Sci.
							<br />⦿ Secured overall 92% in 12th
						</p>
					</VerticalTimelineElement>
					<VerticalTimelineElement
						className="vertical-timeline-element--education"
						contentStyle={{
							background: "rgb(62,68,74)",
							borderTop: "2px solid rgb(33, 150, 243)",
							color: "#FFF",
						}}
						position={"left"}
						date="2015"
						dateClassName="text-white"
						layout="2-column"
						iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
					>
						<h3 className="vertical-timeline-element-title">
							Sunrise English Pvt School
						</h3>
						<h6 className="vertical-timeline-element-subtitle">
							Abu Dhabi, UAE
						</h6>
						<hr />
						<p style={{ fontSize: "16px" }}>
							⦿ School and state topper with a perfect CGPA of 10
						</p>
					</VerticalTimelineElement>
				</VerticalTimeline>
			</Fade>
		</Container>
	);
}

export default About;
