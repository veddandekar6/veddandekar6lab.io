import React from "react";
import { Card, Button } from "react-bootstrap";

function ProjectCard(props) {
  return (
    <Card
      className="bg-dark text-white"
      style={{ textAlign: "center", width: "18rem" }}
    >
      <Card.Img variant="top" src={props.data.img} height={170} />
      <Card.Body>
        <Card.Title>{props.data.title}</Card.Title>
        <Card.Text className="text-justify">{props.data.desc}</Card.Text>
        {props.data.link ? (
          <Button variant="secondary" href={props.data.link} target="_blank">
            Checkout
          </Button>
        ) : (
          " "
        )}
      </Card.Body>
    </Card>
  );
}

export default ProjectCard;
